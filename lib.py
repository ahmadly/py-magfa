#!/usr/bin/env python3
from requests import Session
from requests.auth import HTTPBasicAuth
from zeep import Client
from zeep.cache import SqliteCache
from zeep.transports import Transport
from zeep.plugins import HistoryPlugin
from zeep import xsd


class MagfaSMS:
    """

    """
    session = Session()
    session.auth = HTTPBasicAuth('user', 'pass')

    transport = Transport()
    transport.session = session

    history = HistoryPlugin()

    client = Client(wsdl='http://sms.magfa.com/services/urn:SOAPSmsQueue?wsdl')
    client.transport = transport
    client.plugins = [history]

    factory = client.type_factory('ns1')

    def __init__(self):
        self.domain = 'magfa'
        self.sender_numbers = '300071000'
        self.encodings = 2
        self.udhs = ''
        self.message_classes = 1
        self.priorities = 1

    def _get_credit(self):
        result = self.client.service.getCredit(domain=self.domain)
        return result

    def _enqueue(self, message_bodies, recipient_numbers, checking_message_ids=''):
        result = self.client.service.enqueue(
            domain=self.domain,
            messageBodies=self.factory.ArrayOf_xsd_string([str(message_bodies)]),
            recipientNumbers=self.factory.ArrayOf_xsd_string([str(recipient_numbers)]),
            senderNumbers=self.factory.ArrayOf_xsd_string([str(self.sender_numbers)]),
            encodings=self.factory.ArrayOf_xsd_int([str(self.encodings)]),
            udhs=self.factory.ArrayOf_xsd_string(xsd.SkipValue),
            messageClasses=self.factory.ArrayOf_xsd_int([str(self.message_classes)]),
            priorities=self.factory.ArrayOf_xsd_int([str(self.priorities)]),
            checkingMessageIds=self.factory.ArrayOf_xsd_long([str(checking_message_ids)]),
        )
        return result[0].text

    def send(self, message, mobile, message_id=None):
        return self._enqueue(message_bodies=message, recipient_numbers=mobile, checking_message_ids=message_id)

    def test(self):
        pass
